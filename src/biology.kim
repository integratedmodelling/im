namespace biology
	"Fundamental biological concepts and relationships."
	using im, physical, chemistry
	in domain im:Nature
;

abstract identity TaxonomicIdentity
	has children
		(abstract CommonsenseTaxonomy),
		(abstract Taxonomy);

abstract attribute Sex
	has children
		Hermaphrodite,
		Male,
		Female;

abstract identity Nutrient 
	"Any chemical element or compound that is necessary to the ontogenesis of an
	 individual."
	is chemistry:ChemicalSpecies;

/**
 * These are the GBIF names for the fundamental taxonomic subdivisions. We export them for the 
 * GBIF authority.
 */
abstract identity Kingdom is Taxonomy
	defines authority GBIF.KINGDOM;
	
abstract identity Phylum is Taxonomy
	defines authority GBIF.PHYLUM;
	
abstract identity Class is Taxonomy
	defines authority GBIF.CLASS;
	
abstract identity Order is Taxonomy
	defines authority GBIF.ORDER;
	
abstract identity Family is Taxonomy
	defines authority GBIF.FAMILY;
	
abstract identity Genus is Taxonomy
	defines authority GBIF.GENUS;
	
abstract identity Cultivar is Taxonomy
	defines authority GBIF.CULTIVAR;

abstract identity Species 
	is Taxonomy
	defines authority GBIF.SPECIES;

abstract identity Infraspecies is Taxonomy;

@experimental
// KB Added taxa needed by other models. Some of these will be widely needed across projects and others (sea turtles) may 
// be more limited. These can be moved out of the im ontologies if needed. Note that paraphyletic taxa like fish will
// need to be defined as done below (they don't have a single GBIF identifier) and sea turtles are taxonomically distinct - 
// I don't think it's appropriate to take turtles and apply a realm to them.
/*
 * FV fish is a commonsense taxonomy so no GBIF ID for it - and obviously adding multiple identifiers as it was before 
 * denies the purpose of adding identities. In some cases (like maybe this one) we may want to introduce 
 * intermediate levels w/o identity. TODO complete this to some decent level, not leaving only
 * what we had a use for.
 */
abstract identity Taxon
"A taxon (e.g. superior plants) is used by ecologists in classifying communities but does not have the same rigor as
	 a taxonomic trait."
	has disjoint children
		(Plants inherits GBIF.KINGDOM:6),
		(Insecta inherits GBIF.CLASS:216),
		(Bird inherits GBIF.SPECIES:212)// ,
//		(SeaTurtles identified as "9413" by GBIF.FAMILY);
;
		
agent Individual  
	"The most general biological organism. We make it an agent and self-asserted although it may be a plant or
	 a virus."
//	is physical:SelfAwareBody
	requires identity Species;
	
deniable attribute Reproductive 
	"Attributes the ability to reproduce to an individual." 
	is im:Ability
	applies to Individual;

mass Biomass is im:Mass; //of Individual;

abstract event LifeEvent
	has disjoint children
		EmbryoStage,
		FetusStage,
		Birth,
		InfantStage,
		//FryStage in fishes,
		(JuvenileStage has children 
			Childhood,
			PreAdolescence,
			Adolescence),
		Adulthood, 
		ElderlyStage;

/* should be linked to above using "confers" */		
abstract attribute Adult;
abstract attribute Juvenile;
abstract attribute Fry;

		
event Death 
	is im:Collapse of Individual;

event Life
	"Life is the event of the major life events in this worldview following each other. It provides 
     context for the Growth process."
	is (Death follows Adulthood follows Adolescence follows Childhood follows Birth)
	applies to Individual;
	
process Growth
	"Biological growth only happen during life and affects the biomass of an individual."
	is im:Growth
	affects Biomass within Individual
	applies to Life;

process Reproduction
	""
	applies to Adulthood
//	creates Birth of Individual
	;		
		
// use IUPAC:'chlorophyll a'
//identity ChlorophyllA is chemistry:Compound 
//	// ehm
//	identified as "1S/C55H73N4O5.Mg/c1-13-39-35(8)42-28-44-37(10)41(24-25-48(60)64-27-26-34(7)23-17-22-33(6)21-16-20-32(5)19-15-18-31(3)4)52(58-44)50-51(55(62)63-12)54(61)49-38(11)45(59-53(49)50)30-47-40(14-2)36(9)43(57-47)29-46(39)56-42;/h13,26,28-33,37,41,51H,1,14-25,27H2,2-12H3,(H-,56,57,58,59,61);/q-1;+2/p-1/b34-26+;/t32-,33-,37+,41+,51-;/m1./s1" 
//		by IUPAC;		
		
duration Age
	""
	is im:Current im:Duration of im:Active Life within Individual;
	
agent Plant
	""
	is GBIF.KINGDOM:6 Individual;

agent Insect 
	is GBIF.CLASS:216 Individual;
	
@see(url='www.plantontology.org')
abstract thing PlantPart
//	part of Plant
	has children 
		// TODO - and link to vocs? See www.plantontology.org
		(Flower
			""
		),
		(
			Leaf
			""
		)
;