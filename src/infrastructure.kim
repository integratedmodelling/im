namespace infrastructure
	using im, earth, physical, policy, behavior
	in domain im:Artifacts
;

/* The im.sandobx. infrastructure ontology specifies semantics related to human infrastructure.
 * As this task is really broad, we focus on human infrastructure taxonomy that is relevant
 * for integrated modelling studies.
 * 
 * This classification was based the infrastructure taxonomy by the [Department of Homeland Security][1].
 * Though this classification is not available to the public, and an [older version of 2008][2] was considered .
 * 
 * Of relevance are also: 
 * - [World-Wide Human Geography Data Group (WWHGD)](http://www.geoplatform.gov/human-security-taxonomy).
 * - [Wikipedia](http://en.wikipedia.org/wiki/Infrastructure)
 * 
 * [1]: http://www.dhs.gov/infrastructure-taxonomy.
 * [2]: https://www.hsdl.org/?view&did=14605
 * 
 */
abstract thing GeolocatedInfrastructure is earth:Geolocated physical:Body;
 
// TODO clarify that it is built
abstract thing BuiltArtifact is GeolocatedInfrastructure
	has children
		Building//,
//		Road
//	has policy:Jurisdiction
	;

identity Transportation is im:IntendedPurpose
	applies to im:Movement
	has disjoint children
		(abstract Aviation metadata{
							 dc:comment: "Assets involved in the aviation industry"
							 } 
			has children
			(Airport metadata {
					dc:comment: "Field for handling aircraft landings and takeoffs"
					code:naics: "488119"
				    }),
			(AirTrafficControlCenter metadata{
							    dc:comment: "Includes control centers, radar installations, and communication facilities"
							    code:naics: "488111"
							    }),
		    (Spaceport metadata{
							    dc:comment: "Facilities for launching space vehicles"
		    	})
			),
		 (abstract RailroadInfrastructure metadata {
		 	                dc:comment: "Assets involved in rail trasportation"
		 	                }
   			has children
   			(abstract RailroadConveyance
   				has children
   					FreightConveyance,
   					(PassengerConveyance
   						has children 
   							Intercity,
   						    CommuterTrain)),
   			(abstract RailRightsofWay metadata { 
   									  dc:comment: "Routes along which train operates"
   									  }
		has children
			(RailroadTrack metadata {
					dc:comment: "Incl main line tracks, sidings, switches and crossovers"
					code:naics: "48211"
				    }),
			(RailroadBridge metadata{
							    dc:comment: "Bridges carrying rail traffic. May also carry other kinds of traffic"
							    code:naics: "48211"
							    }),
		    RailroadTunnel,
		    RailroadYard,
		    RailroadStation
	     )
	 );

/*
 * However the above doesn't make much sense from a ARIES point of view.
 * These all are "names" but we cannot assign them to any interpretation.
 * For the infrastructure domain I think we need more than that, as infrastructures
 * provide services to society. And this is done via Networks:
 * 
 * We have Nodes (as Airports, Stations, Harbors, WWTP, river juntions, etc)
 * and Arcs that link the nodes together (as RailroadTracks, Highways, Sewage Networks)/.
 * 
 * Arcs could be omni- or bi-directional
 * 
 * In my opinion if we fail to represent these aspects, there is not much in this 
 * ontology other than a nomenclature of terms.   
 * 
 * The Sewage Infrastructure network consists of arcs that start at Households or 
 * Industries and conclude to a WWTP or an Outlet. Based on our scale it could simply
 * be an arc that connects a City to a WWTP
 * 
 * Similarly, holds for the Electrical Energy Network Infrastructure whose arcs
 * start at PowerPlants and finish in Cities/Households.
 * 
 */
abstract thing TransportationInfrastructure  
	is BuiltArtifact for Transportation
	has disjoint children
		Railway,
		(Road
			has children
				Trail,
				(Highway
					has children
						InterstateHighway,
						StateHighway),
				BikePath,
				LimitedAccessRoad,
				MajorRoad,
				MinorRoad,
				LocalRoad,
				Ramp,
				PedestrianRoad),
		Path,
		FerryRoute,
		Bridge,
		Airfield,
		(Port
			has children
				Anchorage,
				PrimaryNationalPort,
				SecondaryNationalPort,
				PrimaryInternationalPort,
				SecondaryInternationalPort);

attribute RoadSurface
	has disjoint children
		(Unpaved
			has children
				Dirt, //Temporary concepts - shouldn't be nouns - look e.g., at engineering terms (though http://en.wikipedia.org/wiki/Road_surface seems to support use of these terms).
				Gravel),
		(Paved
			has children
				Asphalt,
				Concrete);

abstract thing MonitoringInfrastructure 
	""
	is GeolocatedInfrastructure
	has children
		(@export("IM.WEATHER_STATION")
		 WeatherStation),
		 SamplingSite;

abstract thing CommercialInfrastructure 
	""
	is GeolocatedInfrastructure
	// inherits <purpose>
	// TODO check - activity vs. infrastructure
	has children
		 Bank,
		 Restaurant;

thing CommunicationInfrastructure is GeolocatedInfrastructure
	has children
		CommunicationTower;

thing EnergyInfrastructure
	has children
		(PowerPlant
			has children
				WindTurbine,
				SolarArray,
				HydropowerTurbine
		),
		TransmissionLine,
		(OilGasWell
			has children
				ActiveOilGasWell,
				AbandonedOilGasWell,
				NoOilGasWell);

abstract thing AdministrativeTownship is earth:Location
    has children
	    (
	    	HumanSettlement
	    		"A community in which people live. The complexity of a settlement can range from a small number 
                 of dwellings grouped together to the largest of cities with surrounding urbanized areas. 
                 Settlements include hamlets, villages, towns and cities."
    			has children 
		        	(
		        		Village
		        		"A clustered human settlement or community, larger than a hamlet but smaller 
                         than a town, with a population ranging from a few hundred to a few thousand."
		        	),
       				(
       					Town
       					"A human settlement that is generally larger than a village but smaller and less 
                         organized than a city."
       				),
        			(
        				City
        				"A human settlement with extensive systems for housing, transportation, 
                         sanitation, utilities, land use, and communication."
        			)
         );

thing WorshipBuilding
	"Any building used for worship. Places of worship other than buildings should use
	 the appropriate traits on regions or other features."
	is behavior:Religious Building
	requires identity behavior:Faith
	has children
		(Church inherits behavior:Christian),
		(Synagogue inherits behavior:Jewish),
		(Mosque inherits behavior:Muslim)
		// etc
;

// Dams can be classified, in many different ways (purpose, construction material, etc.)
abstract thing WaterRegulationInfrastructure
	has children
   	    Reservoir,
		Dam,
		Levee,
		DetentionBasin,
		Ditch,
		Aqueduct,
		WaterTreatmentPlant,
		WastewaterTreatmentPlant;

thing ExclusiveEconomicZone is earth:Marine earth:Region;

abstract thing CoastalProtectionInfrastructure
	has children
		Seawall,
		Jetty,
		Groin;

abstract thing HousingInfrastructure
	has children
		House;

@issue(who="ferdinando.villa", what="Does this include mines too?  Can we separate out mines and associated infrastructure (i.e., buildings, roads?)")
thing MiningInfrastructure;

thing Mine;

thing RecreationalArea is policy:Managed earth:Location
    has children
        Playground,
        PicnicArea,
        
        (Accomodation has children
                       AlpineHut,
                       Apartment,
                       Cabin,
                       Campsite,
                       CaravanSite,
                       Chalet,
                       GuestHouse,
                       Hostel,
                       Hotel,
                       Motel,
                       WildernessHut
                       ),
        
        BoatLaunch,
        Trailhead,
        UrbanPark;

@experimental
abstract identity WasteDisposalPurpose
    has children
        Landfill,
        Cesspool;
        
// see http://www.census.gov/eos/www/naics/ for possible authority - as always, with everything jumbled together.
abstract identity Industry 
	"The identity of an industry in the general sense. There may be an authority for these.
	KB: NAICS is a good authority to use - http://www.bls.gov/bls/naics.htm; international authority is International Standard Industrial Classification (http://unstats.un.org/unsd/publication/seriesM/seriesm_4rev4e.pdf)";

abstract thing ManufacturedProduct
	// TODO
	;	
	